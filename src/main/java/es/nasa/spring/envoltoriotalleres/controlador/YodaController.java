package es.nasa.spring.envoltoriotalleres.controlador;

import es.nasa.spring.envoltoriotalleres.cliente.YodaCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

@RestController
@RequestMapping(YodaController.TALLERES+"/*")
public class YodaController {

    private Logger logger = Logger.getAnonymousLogger();
    public static final String TALLERES = "/talleres";

    @Value("${api.starwars.baseurl}")
    private   String HTTPS_SWAPI_DEV_API ;
    @Autowired    private YodaCliente cliente;

    @GetMapping( produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getDatosTalleres(HttpServletRequest request) {
        String fooResourceUrl = crearUrl(request);
        ResponseEntity<String> response = cliente.usaLaFuerza(fooResourceUrl);
        return response;
    }

    @PostMapping(   produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getDatosTalleres(@RequestBody String body, HttpServletRequest request) {

        String fooResourceUrl = crearUrl(request);
        logger.info(body);
        ResponseEntity<String> response = cliente.usaLaFuerza(fooResourceUrl);
        return response;
    }

    private String crearUrl(HttpServletRequest request) {
        String path = request.getServletPath();
                    path =  path.replace(TALLERES,"");
        String fooResourceUrl    = HTTPS_SWAPI_DEV_API+path;
        return fooResourceUrl;
    }

}

