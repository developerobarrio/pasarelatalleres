package es.nasa.spring.envoltoriotalleres;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnvoltoriotalleresApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnvoltoriotalleresApplication.class, args);
    }

}
