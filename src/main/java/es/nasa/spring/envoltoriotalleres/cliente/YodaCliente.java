package es.nasa.spring.envoltoriotalleres.cliente;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class YodaCliente{

        private RestTemplate restTemplate = new RestTemplate();

       public  ResponseEntity<String> usaLaFuerza(String fooResourceUrl) {
            ResponseEntity<String> response =  restTemplate.getForEntity(fooResourceUrl, String.class);
            return response;
        }
}
